#==============================================================================
#     This counts the number of regions (contiguous sites with the same culture)
# in the array 'data' using a modified version of connected components labelling.
# Each unique culture is represent a unique numerical value (henceforth 
# called cultural label).
#==============================================================================

import numpy as np

def parehas(A,B):
    '''checks if two arrays (cultures) are equal'''
    if np.array_equal(A,B):
        return True
    else:
        return False
    
def find_max(region):
    '''finds the maximum cultural label in the 'region' dictionary'''
    array_dct = [region[key] for key in region]
    return np.max(array_dct)
				
def clm(data):
    '''creates a cultural label map'''
    region = {} #will store cultural labels
    N_i,N_j = np.shape(data)[0],np.shape(data)[1]
    for i in xrange(N_i):
        for j in xrange(N_j):
            if i==0 and j==0:
                region = {(0,0):0}
            elif i==0 and j!=0:            
                if parehas(data[i][j],data[i][j-1]): #compare with left only
                    region[(i,j)] = region[(i,j-1)]
                else:
                    region[(i,j)] = find_max(region)+1
            elif i!=0 and j==0:
                if parehas(data[i][j],data[i-1][j]): #compare with top only
                    region[(i,j)] = region[(i-1,j)]
                else:
                    region[(i,j)] = find_max(region)+1
            else:
                bool_top = parehas(data[i][j],data[i-1][j]) #compare with top
                bool_left  = parehas(data[i][j],data[i][j-1]) #compare with left
                bool_tl = parehas(data[i-1][j],data[i][j-1]) #compare left and right neighbors
                if bool_top and not(bool_tl):
                    region[(i,j)] = region[(i-1,j)]
                elif bool_left and not(bool_tl):
                    region[(i,j)] = region[(i,j-1)]
                elif bool_top and bool_tl:
                    bool_label = parehas(region[(i-1,j)], region[(i,j-1)]) #compare label of neighbor
                    if bool_label:
                        region[(i,j)] = region[(i,j-1)]
                    elif region[(i-1,j)] > region[(i,j-1)]: #top label > left label
                        region[(i,j)] =  region[(i,j-1)] #use smaller label
                        for key in region:
                            if region[key] == region[(i-1,j)]:
                                region[key] = region[(i,j-1)]    
                    else:
                        region[(i,j)]  = region[(i-1,j)]
                        for key in region:
                            if region[key] == region[(i,j-1)]:
                                region[key] = region[(i-1,j)]                    
                elif not(bool_top) and not(bool_left):
                    region[(i,j)]  = find_max(region)+1
    label_array = np.empty([np.shape(data)[0],np.shape(data)[1]])
    for key in region:
        label_array[key[0]][key[1]] = region[key]
    return label_array #cultural label map

def count_regions(label_array):
    '''counts the number of cultural regions'''
    cultural_regions = len(np.unique(label_array)) #no. of cultural regions
    return cultural_regions
    