#==============================================================================
#     This contains customized functions for saving images and creating
# slideshow of images.
#==============================================================================

import os
from matplotlib.pyplot import savefig,close
from moviepy.editor import ImageSequenceClip
from moviepy.video.fx.all import resize

def save_png(folder,filename):
    '''saves images as png files in specific folders'''
    script_dir = os.path.dirname(__file__) #current directory
    folder_dir = os.path.join(script_dir, '%s/'%folder) #folder directory
    file_name = filename #name of image file (no need to specify file format)
    
    if not os.path.isdir(folder_dir):
        os.makedirs(folder_dir) #make folder directory if it does not exist
    savefig(folder_dir + file_name,bbox_inches='tight') #saves figure
    close()
    
def save_mp4(folder,filename):
    clip = ImageSequenceClip('%s/'%folder,fps=4) #makes slideshow of images in folder
    clip = resize(clip,newsize=1.5) #change size (scale) of images
    clip.write_videofile('%s.mp4'%filename) #save slideshow as mp4 file
				
				


				
    


