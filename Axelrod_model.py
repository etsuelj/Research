#==============================================================================
#     This contains an implementation of Axelrod's model of cultural
# dissemination. NOTE that to_save.py is needed to save the images and
# slideshows generated, and rehiyon_bilang is needed to count the cultural
# regions.
#==============================================================================

from __future__ import division
import numpy as np
import random
import networkx as nx
import matplotlib.pyplot as plt
from to_save import save_png,save_mp4
from rehiyon_bilang import count_regions,clm

def same_traits(arr1,arr2):
    '''no. of features with same traits in sites arr1 & arr2'''
    st = 0
    for i in xrange(len(arr1)):
        if arr1[i]==arr2[i]:
            st+=1
    return st

def update_sim(edges,c_similarity,N,array):  #updates the cultural similarity
	for i in xrange(len(edges)):
		xs1,ys1,xs2,ys2 = edges[i][0][0],N-1-edges[i][0][1],edges[i][1][0],N-1-edges[i][1][1]
		site1,site2 = array[xs1][ys1],array[xs2][ys2]
		c_similarity[i] = same_traits(site1,site2)/5

def c_map(time,run,N,f,t,trials,imnum,c_lmap):
    '''draw cultural map with agent's culture assigned a color'''
    c_regions = count_regions(c_lmap) #no. of regions in cultural label map
    plt.axis('off')
    plt.suptitle('%i cultural region/s'%c_regions)
    plt.imshow(c_lmap,interpolation='None',origin='lower',cmap='gnuplot')
    plt.title('%i event/s,' %time + '  f=%i,'%f + '  t=%i,'%t + '  %ix%i'%(N,N))
    save_png('Run %s'%run,'%i' %imnum) #saves image
    if time == trials:
        print c_regions
	return c_regions

def neighbor_choice():
    '''choose a random neighbor'''
    #4 adjacent sites
#    galaw = random.sample(zip([0,1,0,-1],[1,0,-1,0]),1)[0]
    #4 adjacent sites + 4 diagonal sites = 8 neighbor sites
#    galaw = [np.random.choice([-1,0,1]),np.random.choice([-1,0,1])]
    #8 adjacent stes + 4 sites two units in cardinal directions			
    galaw = random.sample(zip([0,1,0,-1,1,-1,1,-1,0,2,-2,0],[1,0,-1,0,1,-1,-1,1,2,0,0,-2]),1)[0]
    return galaw[0], galaw[1]

def main(run,trials):
    '''main program'''
    N = 10 #number of sites
    f = 5 #number of cultural features
    t = 10 #number of cultural traits per feature

    #create array with RANDOM traits
    time = 0 #to keep track of the number of events
    imnum = 10000 #to name figures
    array = np.empty([N,N,f])
    for k in xrange(N):
        for i in xrange(N):
            features = np.empty(f)
            for j in xrange(f):
                features[j] = np.random.choice(xrange(t))
            array[k][i] = features
	c_lmap = clm(array) #cultural label map

    G = nx.grid_2d_graph(N,N) #create NxN lattice
    edges = G.edges(data=True)
    c_similarity = np.empty(len(edges)) #will contain cultural overlap of each site
    update_sim(edges,c_similarity,N,array) #update cultural similarity
    c_map(time,run,N,f,t,trials,imnum,c_lmap) #draw cultural map

    for i in xrange(int(trials)):
        time = i+1
        x,y = np.random.choice(xrange(N)),np.random.choice(xrange(N))
        m,n = neighbor_choice()
        while y+n>(N-1) or y+n<0 or x+m>(N-1) or x+m<0: #cultural map is bounded
            m,n = neighbor_choice()
        site,neighbor = array[x][y],array[x+m][y+n] #choose a random site and neighbor

        pili = [1]*same_traits(site,neighbor) + [0]*(f-same_traits(site,neighbor))
        chance = np.random.choice(pili) #1 if feature can change, otherwise 0
        #chance==1: random unlike feature can change
        #same_traits(site,neighbor)!=f: nothing changes, two sites are the same
        if chance==1 and same_traits(site,neighbor)!=f:
            rf = np.random.choice(xrange(f)) #choose a random feature
            while site[rf]==neighbor[rf]: #so random feature is different between neighbors
                rf = np.random.choice(xrange(f))
            site[rf] = neighbor[rf] #change trait of random feature to neighbor's
            c_lmap[x][y] = c_lmap[x+m][y+n] #cultural label is copied
            update_sim(edges,c_similarity,N,array) #update cultural similarity
        if i%(trials/100)==0:
            imnum+=1
            c_map(time,run,N,f,t,trials,imnum,c_lmap) #draw cultural map
    c_regions = c_map(time,run,N,f,t,trials,imnum,c_lmap) #draw cultural map
    return c_regions,c_lmap,N,f,t

num_regions = []
clmap_list = []
for i in xrange(1,11): #run the main code
    run = i
    c_regions,c_lmap,N,f,t = main(run,1.5e5)
    num_regions.append(c_regions)
    clmap_list.append(c_lmap)
    save_mp4('Run %s'%run,'Run %s'%run) #saves slideshow
mean_regions = np.mean(np.array(num_regions))
std_regions = np.std(np.array(num_regions))
params = open("parameters.txt","w")
params.write("N = %i #number of sites \
\nf = %i #number of cultural features \
\nt = %i #number of cultural traits per feature \
\ntime = 150,000 \
\nneighbor = 4 #up, down, left, right\n \
\ncultural regions: "%(N,f,t)) 
params.write(str(num_regions))
params.write('\nMEAN = %f'%mean_regions + '\nSTD = %f'%std_regions)
params.close()

