#==============================================================================
#	Perform a clustering analysis by fast search and find of density peaks.
# See: Rodriguez, A., & Laio, A. (2014). Clustering by fast search and find 
# of density peaks. Science, 344(6191), 1492-1496. doi:10.1126/science.1242072
#==============================================================================

from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
#from scipy.optimize import fmin

x,y,clusternum = np.genfromtxt('Aggregation.txt',delimiter='	',usecols=(0,1,2),unpack=1)
#x,y,clusternum = np.genfromtxt('Compound.txt',delimiter='	',usecols=(0,1,2),unpack=1)
#x,y = np.genfromtxt('cities_municipalities.csv',delimiter=',',usecols=(2,3),unpack=1)
#x,y = np.genfromtxt('phdata.csv',delimiter=',',usecols=(0,1),unpack=1)
x,y = x[np.isnan(x)==False],y[np.isnan(y)==False] #filter data
N = len(x)
kulay = np.loadtxt('colors.dat',dtype=str)


d = np.empty([N,N],float) #distance bet. pts. k & l
for k in xrange(N):
    for l in xrange(k,N):
        d[k,l]=np.sqrt((x[k]-x[l])**2+(y[k]-y[l])**2)
        d[l,k]=d[k,l]
								
print('Setting cut-off distance...')								
frac_n = 0.02
d_sorted = np.sort(d,axis=0)
d_c = np.mean(d_sorted[int(round(frac_n*N))]) #cut-off distance	
print('d_c = %f...'%d_c)

print('Calculating local density of points...')
#rho = np.zeros(N)
#for i in xrange(N): #Heaviside kernel
#    for j in xrange(N):
#        if d[i,j]<d_c:
#            rho[i] = 1.
Gaussian = np.exp(-(d)**2/(2*(d_c**2))) #Gaussian kernel
rho = np.sum(Gaussian,axis=1) #local density	

								
#def H(sigma):
#	rho = np.zeros(N) #local density
#	for i in xrange(N):
#	    for j in xrange(N):
#	        rho[i]+=np.exp((d[i,j])**2/((sigma**2)))
#	Z = np.sum(rho)
#	return -np.sum((rho/Z)*np.log(rho/Z))
#sigma_min = fmin(H,0.)
#sigmas = np.linspace(0,20)
#plt.plot(sigmas,H)
#plt.show()
	
            
maxrho = np.max(rho)
maxrho_loc = np.where(rho==np.max(rho))[0] #point w/ highest density

print('Calculating minimum distance from points of higher density...')
delta = np.empty(N) #min distance from points of higher density
nneigh = np.empty(N,'int')
for k in xrange(N):
    newd = d[k,:]
    if k in maxrho_loc:
        delta[k] = np.max(d[:,maxrho_loc])
    else:
        rho_loc = np.where(rho>rho[k])[0] #indices of higher rho        
        d_sort = newd[rho_loc] #distance of neighbors with higher density
        nneigh[k] = rho_loc[np.argmin(d_sort)] #nearest neighbor with higher density
        delta[k] = np.min(d_sort)

#print('Plotting decision graph...')								
#plt.plot(rho, delta,'c.')
#for k, txt in enumerate(xrange(N)):
#    plt.annotate(txt, (rho[k],delta[k]),fontsize=5)
#plt.title('Decision graph')
#plt.xlabel('$\\rho$',fontsize=15), plt.ylabel('$\delta$',fontsize=15)
#plt.show()

print('\nIdentifying possible cluster centers...')
gamma = delta*rho
grange = np.mean(gamma) + 1*np.std(gamma)
poss_center = np.where(gamma>grange)[0] #possible cluster centers

print('Assigning points to a cluster...')
rho_ord = np.argsort(rho)[::-1] #rho in descending order
cluster_label = np.full(N,-1,dtype='int')
cluster_label[poss_center] = np.arange(len(poss_center))
pts_color = np.chararray(N,itemsize=20)
for r in rho_ord:
    if cluster_label[r] == -1:
        cluster_label[r] = cluster_label[nneigh[r]]
    pts_color[r] =  kulay[cluster_label[r]]

print('Finding the border regions and rho_b...')
rho_b_clusters = []
unique_clusters = np.unique(cluster_label)
for i in xrange(len(unique_clusters)):
    diff_cluster = np.where(cluster_label!=unique_clusters[i])[0] #indices of pts. in other clusters
    in_cluster = np.where(cluster_label==unique_clusters[i])[0] #indices of pts. in the cluster
    if len(diff_cluster)+len(in_cluster) != len(cluster_label):
	     print('ERROR: Something doesn\'t add up')				
    mga_rho_b = []
    for j in diff_cluster:
		within_dc = np.where(d[j]<=d_c)[0] #indices of pts. within d_c
		border = np.intersect1d(within_dc,in_cluster) #indices of border pts.
		if len(border)!=0:		
			mga_rho_b.append(np.max(rho[border]))
    if len(mga_rho_b)!=0:
		rho_b_clusters.append(np.max(mga_rho_b))
    else:
		rho_b_clusters.append(0)

print('Identifying cluster cores and cluster halos...')		
for i in unique_clusters:
	if rho_b_clusters[i]!=0:
		in_cluster = np.where(cluster_label==unique_clusters[i])[0] #indices of pts. in the cluster
		poss_halo = np.where(rho<=rho_b_clusters[i])[0] #indices of pts with rho <= rho_b
		halo = np.intersect1d(poss_halo,in_cluster)
		cluster_label[halo] = -2
		pts_color[halo] = 'gray'
				
print('Drawing scatter plot...')        
plt.scatter(x,y,color=list(pts_color),marker='.')
for i in poss_center:
    plt.plot(x[i],y[i],'ko')
plt.show()