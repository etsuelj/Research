#==============================================================================
# 	This determines the effect of certain parameters on the number of stable
# cultural regions from the original Axelrod model.
#==============================================================================

from __future__ import division
import matplotlib.pyplot as plt

traits = [10,15,20]
traits_reg = [5.1,18.6,44.2]
traits_std = [2.74,7.93,8.65]

plt.style.use('ggplot')
plt.errorbar(traits,traits_reg,yerr=traits_std,fmt='o-')
plt.xlabel('number of traits'), plt.ylabel('number of stable cultural regions')
plt.xlim(0,25)
plt.savefig('traits.png',dpi=300)
plt.close()

feat = [5,7,10]
feat_reg = [5.1,1.6,1.2]
feat_std = [2.74,0.8,0.4]

plt.style.use('ggplot')
plt.errorbar(feat,feat_reg,yerr=feat_std,fmt='o-')
plt.xlabel('number of features'), plt.ylabel('number of stable cultural regions')
plt.xlim(0,15)
plt.savefig('features.png',dpi=300)
plt.close()

n = [3,4,5,6,7,8,9,10]
n_reg = [3.8,5.4,5.3,3.7,4.4,6.9,4.3,5.1]
n_std = [2.14,1.9,1.79,1.27,2.24,3.88,2.53,2.74]
plt.style.use('ggplot')
plt.errorbar(n,n_reg,yerr=n_std,fmt='o-')
plt.xlabel('width of territory'), plt.ylabel('number of stable cultural regions')
plt.xlim(0,11)
plt.savefig('n.png',dpi=300)
plt.close()

scope = [4,8,12]
scope_reg = [5.1,2.2,1.4]
scope_std = [2.74,0.872,0.663]
plt.style.use('ggplot')
plt.errorbar(scope,scope_reg,yerr=scope_std,fmt='o-')
plt.xlabel('range of interaction'), plt.ylabel('number of stable cultural regions')
plt.xlim(0,13)
plt.savefig('range.png',dpi=300)
plt.close()