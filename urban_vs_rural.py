#==============================================================================
# 	Compare the Inglehart-Welzel culture of cities and municipalities
# in the Philippines.
#==============================================================================

from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import ttest_ind,sem
import os

lugar,rehiyon = np.genfromtxt('phdata.csv',delimiter=',',usecols=(17,18),unpack=1,dtype=str)
A,B = np.genfromtxt('phdata.csv',delimiter=',',usecols=(0,1),unpack=1)

B = np.ma.compressed(np.ma.masked_where(np.isnan(A), B))
lugar = [lugar[i][4:] for i in xrange(len(lugar))] #simplify places' names
rehiyon = [rehiyon[i][4:] for i in xrange(len(rehiyon))] #simplify regions' names
lugar = np.ma.compressed(np.ma.masked_where(np.isnan(A),lugar))
rehiyon = np.ma.compressed(np.ma.masked_where(np.isnan(A),rehiyon))
A = np.ma.compressed(np.ma.masked_where(np.isnan(A), A))

u_l,u_li,u_lc = np.unique(lugar,return_index=1,return_counts=1)
rehiyon = [rehiyon[i] for i in u_li]

N_li = len(u_li)
mean_t,mean_s = np.empty(N_li),np.empty(N_li) #mean of dimensions
std_t,std_s = np.empty(N_li),np.empty(N_li) #standard deviation of dimensions
for i in xrange(N_li):
    mean_t[i] = np.mean(A[u_li[i]:u_li[i]+u_lc[i]])
    std_t[i] = np.std(A[u_li[i]:u_li[i]+u_lc[i]])
    mean_s[i] = np.mean(B[u_li[i]:u_li[i]+u_lc[i]])
    std_s[i] = np.std(B[u_li[i]:u_li[i]+u_lc[i]])

set_color = []
for i in xrange(N_li):
	if 'City' in u_l[i] or 'city' in u_l[i]:
		set_color.append('blue') #urban
	else:
		set_color.append('red') #rural

plt.style.use('ggplot')
plt.scatter(mean_s,mean_t,color=set_color)
for k,txt in enumerate(u_l):
	if k not in [16,34,43,31,40,29,44,60,39,21]:
		plt.annotate(txt, (mean_s[k],mean_t[k]),fontsize=5)
plt.ylabel('Traditional                        Secular-rational')
plt.xlabel('Survival                                             Self-expression')
script_dir = os.path.dirname(__file__) #current directory
folder_dir = os.path.join(script_dir, 'Figures/') #folder directory
file_name = 'urban vs rural' #name of image file (no need to specify file format)    
if not os.path.isdir(folder_dir):
	os.makedirs(folder_dir) #make folder directory if it does not exist
plt.savefig(folder_dir+file_name,dpi=300,bbox_inches='tight')
plt.close()

urban_t,urban_s = [],[]
rural_t,rural_s = [],[]
for i in xrange(N_li):
	if set_color[i] == 'blue':
		urban_t.append(mean_t[i])
		urban_s.append(mean_s[i])
	else:
		rural_t.append(mean_t[i])
		rural_s.append(mean_s[i])
print '\nmean:\n',np.mean(urban_t),np.mean(urban_s),np.mean(rural_t),np.mean(rural_s)
print '\nSEM:\n',sem(urban_t),sem(urban_s),sem(rural_t),sem(rural_s)

data = zip(u_l,rehiyon,mean_t,mean_s,std_t,std_s,set_color)
np.savetxt('cities_municipalities.csv',data,delimiter=',',fmt='%s',header='Cities & municipalities,region,tradrat (mean), survself (mean), tradrat (std), survself (std),color (urban vs. rural)')
		

'''t-test for two independent samples of scores (t = 2.00 for df=60)'''
trad_t = ttest_ind(urban_t,rural_t,equal_var=0)
surv_t = ttest_ind(urban_s,rural_s,equal_var=0) 
print '\nt-statistics:\n',trad_t[0],surv_t[0]